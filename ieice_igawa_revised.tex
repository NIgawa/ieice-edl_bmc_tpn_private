%#!latex

\documentclass[letter]{ieice}

\usepackage{amssymb}
\usepackage{amsmath}
%\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{url}
% Basically, \url{my_url_here}.
\usepackage{cite}
\usepackage{algorithmic}
\usepackage{multirow}


\field{}
%\SpecialIssue{}
%\SpecialSection{}
%\theme{}
\title{Symbolic Representation of Time Petri Nets for Efficient Bounded Model Checking}
%\title[title for header]{title}
%\titlenote{}
\authorlist{%
	\authorentry{Nao IGAWA}{n}{OPU}\MembershipNumber{}
	\authorentry{Tomoyuki YOKOGAWA}{m}{OPU}\MembershipNumber{0522848}
	\authorentry{Sousuke AMASAKI}{n}{OPU}\MembershipNumber{}
	\authorentry{Masafumi KONDO}{m}{KMW}\MembershipNumber{0835072}
	\authorentry{Yoichiro SATO}{m}{OPU}\MembershipNumber{8208475}
	\authorentry{Kazutami ARIMOTO}{m}{OPU}\MembershipNumber{8307437}
}
\affiliate[OPU]{Okayama Prefectural University, 111 Kuboki, Soja, Okayama 719-1197, Japan}
\affiliate[KMW]{Kawasaki University of Medical Welfare, 288 Matsushima, Kurashiki, Okayama 701-0193, Japan}

%\paffiliate[present affiliate label]{Presently, the author is with the }

\received{2015}{1}{1}
\revised{2015}{1}{1}


\begin{document}

\maketitle 

\begin{summary}
Safety critical systems are often modeled using Time Petri Net (TPN) for analyzing its reliability with formal verification methods.
This paper proposed a efficient verification method for TPN introducing bounded
model checking based on satisfiability solving.
The proposed method expresses time constraints of TPN by Difference Logic (DL) and uses an SMT solver for verification.
Its effectiveness was also demonstrated with an experiment.
\end{summary}

\begin{keywords}
formal verification, Time Petri Net, SMT Solver, difference logic
\end{keywords}


\section{Introduction}
Industries strive for assuring their safety critical systems, failure of which exposes man-life and social infrastructures to serious danger.
For assurance, the safety critical systems are often modeled using Time Petri Net (TPN)~\cite{Bernardi2011}\cite{Kacem2012}, which is one of the main extensions of Petri Nets with time constraints~\cite{Merlin1976a}.
The reliability of systems is then analyzed with the TPN models applying formal verification methods.
A formal verification approach based on model checking methodology expresses possibly tremendous states of a system as assignments to propositional variables and real ones.
Thus, tools based on this approach such as TINA \cite{Berthomieu2004} and Romeo \cite{Lime2009} are suffered from a complex and space consuming problem.
Representing the state space symbolically such as~\cite{Audemard2002}\cite{Sorea2003}\cite{Chen2017} is considered promising.
These studies applied SAT-based bounded model checking~\cite{Biere1999} to the verification of Timed Automata.

In \cite{Yokogawa2015a}, we proposed a verification method for TPN based on bounded model checking that extended an SAT-based bounded model checking method for Petri Nets~\cite{Ogata2004a} and showed the advantage of our method over existing model checker.
The method expressed time constraints in linear arithmetic (LA) and enabled to use an SMT (Satisfiability Modulo Theories) solver for verification.
Its symbolic representation adopted a similar encoding of timed systems in~\cite{Sorea2003} such that firing of transitions and elapsing place delays were encoded as boolean expressions separately.
We also extended the symbolic representation to express time constraints in difference logic (DL) \cite{Igawa2018} in a similar manner to \cite{Chen2017}, and an efficient satisfiability solving algorithm for formulas described in DL~\cite{Cotton2006} could be applicable.

In this paper, we conduct a comparison experiment and show the advantage of the encoding based on DL compared to the existing encoding based on LA.
We prepare different-sized TPN examples to evaluate the scalability of the encoding.
We also demonstrate that the formula size reduction \cite{Yokogawa2015a} has a greater effect on the proposed encoding.

\section{Bounded Model Checking for TPN}
\subsection{TPN}
We introduce P-Time Petri Nets (P-TPN)~\cite{Sifakis1980}, a subclass of TPN that extend Petri Nets (PN) so that it can represent a place delay.
P-TPN is defined as 6-tuple P-TPN $= (P, T, F, F_{in}, M_0, X)$, where $P$ is a set of {\em places},
 $T$ is a set of {\em transitions},
 $F \subseteq(P \times T) \cup (T \times P)$ is a set of {\em arcs}, $F_{in} \subset (P \times T)$ is a set of {\em inhibitor arcs}
 and $M_0 \subseteq P$ is an initial marking.
$X:P \rightarrow (\mathbb{Z}^+) \times (\mathbb{Z}^+ \cup +\inf)$ is a function mapping places to place delays
 ($\mathbb{Z}^+$ denotes a set of integers which are greater than or equal to zero).
We focus on a {\em safe} P-TPN where each place has at most one token, and no arc weight is considered.

A place can have a {\em token}.
A place delay represents a time required to enable a token of the place and have lower bound $l_i$ and upper bound $u_i$ ($l_i \leq u_i$) for $p_i \in P$.
A token in place $p_i$ can be enabled after $l_i$ elapsed and must be enabled until $u_i$ elapsed.
A transition can fire when all of its input places have an {\em enabled} token, and the tokens move to its output places through the transition.
An inhibitor arc represents a condition of inhibiting a transition firing.
Transitions connected with input places through an inhibitor arc can fire only if those places have no enabled token.
Input and output places of a transition $t \in T$ are described as $\bullet t$ and $t \bullet$, 
 and input and output transitions of a place $p \in P$ are described as $\bullet p$ and $p \bullet$, respectively.
Places with an inhibitor arc to transition $t$ are represented as $\circ t$.



\subsection{Bounded Model Checking}
A state $s$ of TPN which has $l$ places ($l = |P|$) is defined by a variable $c$ and two $l$ - vectors $\textbf{m} = (m_1,\ldots,m_l)$ and $\textbf{z} = (z_1,\ldots, z_l)$ for the vector of places $\textbf{p} = (p_1,\ldots,p_l)$ where $p_i \in P$.
$c$ denotes the global time of the TPN and $z_i$ represents the time when $p_i$ gets a token.
By using the variables, the elapsed time of the token in $p_i$ is expressed as $c - z_i$.
$m_i$ is a boolean variable which evaluates to true when $p_i$ has a token.
%Here $s \overset{t}{\rightarrow} s^{\prime}$ denotes that a state $s$ changes to $s^{\prime}$ by a firing of a transition $t$, and $s \overset{x}{\rightarrow} s^{\prime}$ denotes that a state $s$ changes to $s^{\prime}$ by elapsing of time interval $x$.
Here, a boolean function over variables of $s$ which holds iff a state $s$ belongs to a state set $S$ is called a \textit{characteristic function} of $S$.
Similarly, a transition relation can be specified as a characteristic function over $s$ and $s^{\prime}$ which holds iff $s$ can change to $s^{\prime}$.

To carry out SAT-based bounded model checking, it is necessary to encode two characteristic functions $\mathcal{N}_k$ and $\mathcal{R}_k$.
$\mathcal{N}_k(s_0,\cdots,s_k)$ denotes that the initial state $s_0$ can reach $s_k$ by $k$-steps through $s_1, s_2, \cdots , s_{k-1}$.
$\mathcal{R}_k(s_0,\cdots,s_k)$ denotes that a desirable property to be checked is satisfied in any one of the states $s_0, \cdots, s_k$.
Here, a \textit{step} represents the changing of states caused by time elapsing and firing of a transition.
%Formally, a step is defined as a changing of states $s$ and $s^{\prime}$, where $s \overset{x}{\rightarrow} s^{\prime\prime}$ and $s^{\prime\prime} \overset{t}{\rightarrow} s^{\prime}$ for some $t$, $x$, and $s^{\prime\prime}$.
A step is defined as a changing of states $s$ and $s^{\prime}$ through a state $s^{\prime\prime}$,
 where $s$ changes to $s^{\prime\prime}$ by elapsing of some time interval and $s^{\prime\prime}$ changes to $s^{\prime}$ by a firing of some transition.
Bounded model checking can check whether a system can satisfy the desirable property within $k$-steps by determining the satisfiability of $\mathcal{N}_k \wedge \mathcal{R}_k$ using SMT-solver.
If $\mathcal{N}_k \wedge \mathcal{R}_k$ is satisfiable, the property can be satisfied in $k$-steps from the initial state.
That is, the system satisfies the reachability of the states where the property holds, and the verification succeeds.
If not, it means that the system does not satisfy the property at least within $k$-steps.



\subsection{Symbolic Representation}
In our encoding, time constraints are expressed in difference logic (DL), which is a sub logic of linear arithmetic and has a form restricted as $x - y \bowtie c$ for variables $x$, $y$ and constant $c$, where $\bowtie$ represent equality or inequality.
Constraints expressed in DL can be solved efficiently by searching a negative cycle of a weighted directed graph.

Two step types are supposed in our encoding: an \textit{elapsing sub-step} caused by time elapsing and a \textit{firing sub-steps} caused by transition firing discretely.
$\mathcal{C}(s, s^{\prime})$ and $\mathcal{F}(s, s^{\prime})$ denote characteristic functions of an elapsing sub-step and a firing sub-steps.
A characteristic function of one step $\mathcal{T}(s, s^{\prime})$ is defined as $\mathcal{T}(s, s^{\prime}) \overset{def}{=} \mathcal{C}(s, s^{\prime\prime}) \wedge \mathcal{F}(s^{\prime\prime}, s^{\prime})$ for some intermediate state $s^{\prime\prime}$ between the steps.

A transition $t$ can fire when all $p \in \bullet t$ have enabled tokens and any $p \in \circ t$ has no enabled token; otherwise, $t$ can not fire.
Here, we introduce two characteristic functions $En_t(s)$ and $Ds_t(s)$.
$En_t(s)$ denotes $t$ can fire in a state $s$ and $Ds_t(s)$ denotes $t$ can not fire in a state $s$ as follows:
\footnotesize\begin{align*}
En_t(s) \overset{def}{=}& \bigwedge_{p_i \in \bullet t} (m_i \wedge u_i \leq c - z_i) \wedge \bigwedge_{p_i \in \circ t} \neg (m_i \wedge l_i \leq c - z_i),\\
Ds_t(s) \overset{def}{=}& \bigvee_{p_i \in \bullet t} \neg (m_i \wedge l_i \leq c - z_i) \vee \bigvee_{p_i \in \circ t} (m_i \wedge u_i \leq c - z_i).
\end{align*}\normalsize
Note that $En_t(s)$ and $Ds_t(s)$ are defined so that there exist states where neither $En_t(s)$ nor $Ds_t(s)$ holds.
This is because whether the token in $p_i$ is enabled or not is decided non-deterministically when $l_i < c - z_i < u_i$.
In such states, whether $t$ fires or not is also decided non-deterministically.

As an elapsing sub-step is performed when no transition can fire, $\mathcal{C}(s, s')$ can be defined as follows:
\footnotesize\begin{align*}
\mathcal{C}(s, s^{\prime}) \overset{def}{=}&  (c^{\prime} - c > 0) \wedge \bigwedge_{t \in T} \neg En_t(s)
 \wedge \bigwedge_{p_i \in P} (z^{\prime}_i = z_i \wedge m^{\prime}_i \leftrightarrow m_i)\\
& \vee (c^{\prime} - c = 0) \wedge \bigwedge_{p_i \in P} (z^{\prime} = z_i \wedge m^{\prime}_i \leftrightarrow m_i)
\end{align*}\normalsize
where $m^{\prime}_i$ and $z^{\prime}_i$ represent $m_i$ and $z_i$ on state $s^{\prime}$ respectively.

$\mathcal{F}(s, s^{\prime})$ can be defined as a conjunction of characteristic functions $F_t(s_{i}, s_{j})$ for all transitions ($n = |T|$), which holds iff $s_{i}$ changes to $s_{j}$ by a firing of $t$ or $s_{i} = s_{j}$:
\footnotesize\begin{align*}
\mathcal{F}(s, s^{\prime}) \overset{def}{=} F_{t_1}(s, s_1) \wedge F_{t_2}(s_1, s_2) \wedge \cdots \wedge F_{t_n}(s_{n-1}, s^{\prime}).
\end{align*}\normalsize
$\mathcal{F}(s, s^{\prime})$ holds if $s$ can reach $s'$ by firing of some sequence of transitions following the order in $t_1, \ldots, t_n$.
$F_t(s, s^{\prime})$ can be defined as follows:
\footnotesize\begin{align*}
\mathcal{F}_t(s, s^{\prime}) \overset{def}{=}& (c^{\prime} - c = 0) \wedge \neg Ds_t(s)\\
& \wedge \bigwedge_{p_i \in t \bullet} (m^{\prime}_i \wedge z^{\prime}_i = c) \wedge \bigwedge_{p_i \in \bullet t \backslash t \bullet} (\neg m^{\prime}_i \wedge z^{\prime}_i = z_i)\\
& \wedge \bigwedge_{p_i \in P \backslash (\bullet t \cup t \bullet)} (m^{\prime} \leftrightarrow m_i \wedge z^{\prime} = z_i) \\
& \vee (c^{\prime} - c = 0) \wedge \bigwedge_{p_i \in P} (m^{\prime}_i \leftrightarrow m_i \wedge z^{\prime}_i = z_i).
\end{align*}\normalsize


Since $\mathcal{N}_k$ represents that the initial state $s_0$ can reach the state $s_k$ by $k$-steps, $\mathcal{N}_k$ can be obtained as follows:
\footnotesize\begin{align*}
\mathcal{N}_k \overset{def}{=} \mathcal{I}(s_0) \wedge \mathcal{T}(s_0, s_1) \wedge \mathcal{T}(s_1, s_2) \wedge \cdots \wedge \mathcal{T}(s_{k-1}, s_k)
\end{align*}\normalsize
where $\mathcal{I}(s)$ denotes a characteristic function of the initial states and is defined as follows:
\footnotesize\begin{align*}
\mathcal{I}(s) \overset{def}{=} \bigwedge_{p_i \in M_0} m_i \wedge \bigwedge_{p_i \notin M_0} \neg m_i \wedge \bigwedge_{p_i \in P} z_i = 0.
\end{align*}\normalsize


Suppose a characteristic function $R(s)$ which holds iff a given property is satisfied in $s$.
When $\mathcal{N}_k$ holds for the states $s_0, \cdots, s_k$ and $R(s_i)$ holds in some state $s_i$ ($0 \le i \le k$), $\mathcal{N}_k$ also holds by assigning $s_i$ to $s_{i+1}, \ldots, s_k$.
Because $\mathcal{T}(s, s^{\prime})$ is true when $s = s^{\prime}$.
Thus we can obtain $\mathcal{R}_k \overset{\mathrm{def}}{=} R(s_k)$.

For example, deadlock freeness of TPN can be checked by expressing deadlock states as a characteristic function.
A deadlock occurs when all tokens of places are enabled and no transition can fire.
Thus a characteristic function $R_d(s)$ which denotes deadlock states can be defined as follows:
\footnotesize\begin{align*}
R_d(s) \overset{def}{=} \bigwedge_{t \in T} \neg En_t(s) \wedge \bigwedge_{p_i \in P} (m_i \rightarrow u_i \leq c - z_i).
\end{align*}\normalsize

\subsection{Size of Formula to be Solved}
To express time constraints in DL, we introduce the additional variable $c$ which represents the global time for each state and the additional constraints over the value of $c$.
Thus the formula to be solved increases in the number of variables.
In \cite{Yokogawa2015a}, we provided the method for reducing formula size by replacing unchanged variables.
Since the value of $z_i$ will be unchanged until the place $p_i$ gets a new token in the proposed encoding,
 there are many terms formed as $(z^{\prime}_i = z_i)$ which can be removed by replacing $z^{\prime}_i$ with $z_i$.
This makes it possible for the proposed encoding to reduce the formula size compared to the existing encoding.


\section{Experiments}
For evaluation, the proposed encoding was compared with the existing encoding based on Linear Arithmetic (LA) shown in \cite{Yokogawa2015a}.
This experiment was performed on the computer with Ubuntu 16.04 LTS OS, Intel Core i7 7700 3.6 GHz CPU, and 64 GB Memory.
We supplied three TPNs \#1, \#2 and \#3, whose numbers of places/transitions are 374/389, 1030/1193 and 3206/4049.
We used five SMT solvers at the top of SMT-COMP 2018 \cite{SMT-COMP2018}:
 MathSAT \cite{Bruttomesso2008}, Z3 \cite{DeMoura2008}, SMTInterpol \cite{Christ2012}, yices \cite{Dutertre2014}, and CVC4 \cite{Barrett2011}.

Table \ref{comparison2} shows the comparison of effects of the formula size reduction by variable replacing.
``Variable'' denotes the variables which are declared in the formula, and ``constraint'' denotes the logical constraints in the formula.
Here the logical constraint denotes $x$, $ \neg x$, or $x \leftrightarrow y$ for boolean variable $x$ and $y$, or constraints in LA.
As shown in the Table, the number of variables and constraints can be significantly reduced by the variable replacing, and the reduction works well on the proposed encoding compared to the existing one.

\begin{table*}[t]
	\centering
	\footnotesize
	\caption{Effects of variables replacing.}
	\label{comparison2}
	\begin{tabular}{ccrrrrrrrr}
		\hline\hline
		&      & \multicolumn{4}{c}{\# of variables}                                        & \multicolumn{4}{c}{\# of constraints}                                         \\
		& Step & \multicolumn{2}{c}{before replacing} & \multicolumn{2}{c}{after replacing} & \multicolumn{2}{c}{before replacing} & \multicolumn{2}{c}{after replacing} \\
		& 		& \multicolumn{1}{c}{LA}	& \multicolumn{1}{c}{DL}	& \multicolumn{1}{c}{LA}	& \multicolumn{1}{c}{DL}	& \multicolumn{1}{c}{LA}	& \multicolumn{1}{c}{DL}	& \multicolumn{1}{c}{LA}	& \multicolumn{1}{c}{DL}	\\
		\hline
		%		& 1    & 44,352            & 44,496           & 1,435            & 1,282            &      48,634         &      48,477        &     5,408            &     4,954  \\
		%		& 2    & 88,704            & 88,992           & 2,562            & 2,255            &      95,624         &      95,456        &     9,172            &     8,410  \\
		%		& 3    & 133,056           & 133,488          & 3,689            & 3,228            &     142,614         &     142,435        &    12,936            &    11,866  \\
		%		& 4    & 177,408           & 177,984          & 4,816            & 4,201            &     189,604         &     189,414        &    16,700            &    15,322  \\
		%	\#1 & 5    & 221,760           & 222,480          & 5,943            & 5,174            &     236,594         &     236,393        &    20,464            &    18,778  \\
		%		& 6    & 266,112           & 266,976          & 7,070            & 6,147            &     283,584         &     283,372        &    24,228            &    22,234  \\
		%		& 7    & 310,464           & 311,472          & 8,197            & 7,120            &     330,574         &     330,351        &    27,992            &    25,690  \\
		%		& 8    & 354,816           & 355,968          & 9,324            & 8,093            &     377,564         &     377,330        &    31,756            &    29,146  \\
		%		& 9    & 399,168           & 400,464          & 10,451           & 9,066            &     424,554         &     424,309        &    35,520            &    32,602  \\
		%		& 10   & 443,520           & 444,960          & 11,578           & 10,039           &     471,544         &     471,288        &    39,284            &    36,058  \\
		%		\hline
		& 1    & 291,720           & 292,110          & 3,815            & 3,442            &     303,033         &     302,688        &    14,379            &    13,271  \\
		%		& 2    & 583,440           & 584,220          & 6,882            & 6,135            &     601,910         &     601,580        &    24,602            &    22,746  \\
		%		& 3    & 875,160           & 876,330          & 9,949            & 8,828            &     900,787         &     900,472        &    34,825            &    32,221  \\
		%		& 4    & 1,166,880         & 1,168,440        & 13,016           & 11,521           &   1,199,664         &   1,199,364        &    45,048            &    41,696  \\
		\#1 & 5    & 1,458,600         & 1,460,550        & 16,083           & 14,214           &   1,498,541         &   1,498,256        &    55,271            &    51,171  \\
		%		& 6    & 1,750,320         & 1,752,660        & 19,150           & 16,907           &   1,797,418         &   1,797,148        &    65,494            &    60,646  \\
		%		& 7    & 2,042,040         & 2,044,770        & 22,217           & 19,600           &   2,096,295         &   2,096,040        &    75,717            &    70,121  \\
		%		& 8    & 2,333,760         & 2,336,880        & 25,284           & 22,293           &   2,395,172         &   2,394,932        &    85,940            &    79,596  \\
		%		& 9    & 2,625,480         & 2,628,990        & 28,351           & 24,986           &   2,694,049         &   2,693,824        &    96,163            &    89,071  \\
		& 10   & 2,917,200         & 2,921,100        & 31,418           & 27,679           &   2,992,926         &   2,992,716        &   106,386            &    98,546  \\
		\hline
		& 1    & 2,459,640         & 2,460,834        & 11,479           & 10,450           &   2,494,281         &   2,493,440        &    44,059            &    40,995  \\
		%		& 2    & 4,919,280         & 4,921,668        & 20,898           & 18,839           &   4,976,196         &   4,975,518        &    75,752            &    70,628  \\
		%		& 3    & 7,378,920         & 7,382,502        & 30,317           & 27,228           &   7,458,111         &   7,457,596        &   107,445            &   100,261  \\
		%		& 4    & 9,838,560         & 9,843,336        & 39,736           & 35,617           &   9,940,026         &   9,939,674        &   139,128            &   129,894  \\
		\#2 & 5    & 12,298,200        & 12,304,170       & 49,155           & 44,006           &  12,421,941         &  12,421,752        &   170,831            &   159,527  \\
		%		& 6    & 14,757,840        & 14,765,004       & 58,574           & 52,395           &  14,903,856         &  14,903,830        &   202,524            &   189,160  \\
		%		& 7    & 17,217,480        & 17,225,838       & 67,993           & 60,784           &  17,385,771         &  17,385,908        &   234,217            &   218,793  \\
		%		& 8    & 19,677,120        & 19,686,672       & 77,412           & 69,173           &  19,867,686         &  19,867,986        &   265,910            &   248,426  \\
		%		& 9    & 22,136,760        & 22,147,506       & 86,831           & 77,562           &  22,349,601         &  22,350,064        &   297,603            &   278,059  \\
		& 10   & 24,596,400        & 24,608,340       & 96,250           & 85,951           &  24,831,516         &  24,832,142        &   329,296            &   307,692  \\
		\hline
		& 1    & 25,968,600        & 25,972,650       & 38,423           & 35,218           &  26,085,323         &  26,083,010        &   148,733            &   139,165  \\
		%		& 2    & 51,937,200        & 51,945,300       & 70,434           & 64,023           &  52,129,891         &  52,128,421        &   256,711            &   240,731  \\
		%		& 3    & 77,905,800        & 77,917,950       & 102,445          & 92,828           &  78,174,459         &  78,173,832        &   364,689            &   342,297  \\
		%		& 4    & 103,874,400       & 103,890,600      & 134,456          & 121,633          & 104,219,027         & 104,219,243        &   472,667            &   443,863  \\
		\#3 & 5    & 129,843,000       & 129,863,250      & 166,467          & 150,438          & 130,263,595         & 130,264,654        &   580,645            &   545,429  \\
		%		& 6    & 155,811,600       & 155,835,900      & 198,478          & 179,243          & 156,308,163         & 156,310,065        &   688,623            &   646,995  \\
		%		& 7    & 181,780,200       & 181,808,550      & 230,489          & 208,048          & 182,352,731         & 182,355,476        &   796,601            &   748,561  \\
		%		& 8    & 207,748,800       & 207,781,200      & 262,500          & 236,853          & 208,397,299         & 208,400,887        &   904,579            &   850,127  \\
		%		& 9    & 233,717,400       & 233,753,850      & 294,511          & 265,658          & 234,448,926         & 234,446,298        & 1,012,557            &   951,693  \\
		& 10   & 259,686,000       & 259,726,500      & 326,522          & 294,463          & 260,494,216         & 260,491,709        & 1,120,535            & 1,053,259  \\
		\hline\hline
	\end{tabular}
\end{table*}

Table \ref{comparison} shows execution times of bounded model checking from 1 step to 10 steps for the three TPNs.
The column ``LA'' and ``DL'' describes the execution times by the existing encoding and proposed encoding, respectively.
We set the timeout to 600 seconds.
As shown in the ``result'' column, deadlock cannot be detected for the TPNs within 10 steps.
For all of the SMT solvers, the proposed encoding performed better than the existing encoding.
In particular, yices showed a good performance for the formula in DL even if the size of the target TPN becomes large.

\begin{table*}[t]
	\centering
	\footnotesize
	\caption{Comparison of execution time (sec.).}
	\label{comparison}
	\begin{tabular}{ccrrrrrrrrrrc}
		\hline\hline
		& \multicolumn{1}{c}{Step} & \multicolumn{2}{c}{MathSAT}	& \multicolumn{2}{c}{Z3}	& \multicolumn{2}{c}{SMTIinterpol}	& \multicolumn{2}{c}{yices}	&	\multicolumn{2}{c}{CVC4} & result \\
		%\cline{3-12}
		& 		& \multicolumn{1}{c}{LA}	& \multicolumn{1}{c}{DL}	& \multicolumn{1}{c}{LA}	& \multicolumn{1}{c}{DL}	& \multicolumn{1}{c}{LA}	& \multicolumn{1}{c}{DL}	& \multicolumn{1}{c}{LA}	& \multicolumn{1}{c}{DL}	& \multicolumn{1}{c}{LA}	& \multicolumn{1}{c}{DL}	&	\\
		\hline
		%	& 1		&   0.034	&   0.024	&   0.037	&   0.025	&  1.389	&   0.743	&   0.228	&   0.007	&   0.342	&   0.110	& sat	\\
		%	& 2		&   0.267	&   0.058	&   0.106	&   0.075	&  3.886	&   1.855	&   0.859	&   0.014	&   1.220	&   0.401	& sat	\\
		%	& 3		&   0.965	&   0.206	&   0.241	&   0.129	&  3.137	&   2.411	&   4.349	&   0.020	&   4.950	&   1.441	& sat	\\
		%	& 4		&   0.305	&   0.256	&   0.398	&   0.335	&  9.448	&   4.518	&  34.916	&   0.052	&   7.327	&   4.272	& sat	\\
		%\#1 & 5		&   1.512	&   0.689	&   0.835	&   0.905	& 11.917	&   7.187	&  96.957	&   0.051	&  33.912	&   7.341	& sat	\\
		%	& 6		&   7.400	&   2.310	&   1.270	&   1.797	& 48.393	&   8.559	& 212.533	&   0.048	&  41.528	&  14.395	& sat	\\
		%	& 7		&  14.667	&   2.175	&   1.755	&   2.494	& 21.073	&  19.718	& 494.693	&   0.055	&  27.020	&  27.556	& sat	\\
		%	& 8		&  11.195	&   5.305	&   7.698	&   3.906	& 53.242	&   8.305	& $>$ 600	&   0.098	& 114.706	&  35.196	& sat	\\
		%	& 9		&  20.433	&   5.170	&   4.146	&   4.138	& 43.010	&  13.084	& ---		&   0.152	& 134.912	&  41.284	& sat	\\
		%	& 10	&  51.503	&  14.931	&   6.647	&  15.796	& 56.567	&  15.857	& ---		&   1.485	& 106.905	&  59.701	& sat	\\
		%\hline
		& 1		&   0.043	&   0.031	&   0.031	&   0.031	&   0.479	&   0.420	&   0.012	&   0.015	&   0.149	&   0.148	& unsat	\\
		& 2		&   0.069	&   0.061	&   0.071	&   0.071	&   0.629	&   0.570	&   0.023	&   0.027	&   0.277	&   0.270	& unsat	\\
		& 3		&   0.118	&   0.116	&   0.138	&   0.137	&   0.730	&   0.690	&   0.035	&   0.041	&   0.421	&   0.405	& unsat	\\
		& 4		&   0.219	&   0.186	&   0.250	&   0.238	&   0.941	&   0.891	&   0.048	&   0.057	&   0.544	&   0.524	& unsat	\\
		\#1	& 5		&   4.182	&   1.097	&   0.965	&   0.785	&   6.158	&   8.586	&   6.397	&   0.209	&   1.113	&   1.008	& unsat	\\
		& 6		&   5.325	&   2.876	&   2.989	&   1.813	&  11.633	&  11.916	&  16.283	&   0.544	&   2.860	&   1.406	& unsat	\\
		& 7		&  18.612	&   3.956	&   8.524	&   3.264	&  17.235	&  39.502	&  12.725	&   0.590	&   3.395	&   2.454	& unsat	\\
		& 8		&  19.148	&  10.185	&  14.632	&   5.260	&  78.673	&  58.682	&  14.222	&   1.393	&  11.403	&   4.746	& unsat	\\
		& 9		&  31.172	&  17.919	&  33.772	&  11.692	& 201.717	& 187.003	&  32.164	&   3.885	&  49.292	&  14.752	& unsat	\\
		& 10	&  78.780	&  42.962	&  61.003	&  32.251	& $>$ 600	& 258.586	&  91.310	&   7.780	&  69.900	& 184.923	& unsat	\\
		\hline
		& 1		&   0.103	&   0.108	&   0.107	&   0.104	&   0.888	&   0.843	&   0.038	&   0.046	&   0.620	&   0.549	& unsat	\\
		& 2		&   0.353	&   0.283	&   0.384	&   0.333	&   2.880	&   1.499	&   0.089	&   0.101	&   1.304	&   1.143	& unsat	\\
		& 3		&   0.878	&   0.557	&   1.040	&   0.717	&   2.187	&   2.130	&   0.343	&   0.173	&   1.987	&   1.754	& unsat	\\
		& 4		&   1.799	&   1.082	&   1.775	&   1.343	&  10.130	&   6.396	&   0.854	&   0.291	&   2.762	&   2.418	& unsat	\\
		\#2	& 5		&   6.991	&   4.115	&   4.165	&   4.024	&  16.678	&  35.643	&   4.230	&   0.427	&   3.969	&   3.582	& unsat	\\
		& 6		&  10.310	&   6.721	&   8.226	&   6.014	&  55.335	&  62.101	&   9.524	&   1.229	&  11.929	&   5.239	& unsat	\\
		& 7		&  40.479	&  46.537	&  29.248	&  14.886	& 148.538	& 294.035	&  42.590	&   4.308	&  20.572	&  10.864	& unsat	\\
		& 8		&  65.158	&  68.318	&  77.255	&  37.106	& 591.304	& 320.213	&  71.035	&   8.127	&  65.388	&  29.093	& unsat	\\
		& 9		&  88.706	&  53.858	& 118.414	&  84.530	& $>$ 600	& $>$ 600	& 212.159	&  19.547	& 208.174	&  73.094	& unsat	\\
		& 10	& 296.510	& 173.050	& 224.211	& 135.838	& ---		& ---		& 340.504	&  48.888	& 389.901	& 204.180	& unsat	\\
		\hline
		& 1		&   0.371	&   0.384	&   0.437	&   0.415	&   1.867	&   1.775	&   0.129	&   0.160	&   1.837	&   1.827	& unsat	\\
		& 2		&   1.727	&   1.520	&   2.706	&   1.715	&  10.087	&  10.064	&   0.468	&   0.460	&   4.804	&   4.819	& unsat	\\
		& 3		&   5.398	&   3.688	&   7.790	&   4.658	&  23.477	&  19.291	&   2.835	&   1.011	&   7.868	&   7.157	& unsat	\\
		& 4		&  11.624	&   7.474	&  16.579	&   9.238	& 130.407	& 104.760	&  26.152	&   2.488	&  10.562	&   9.784	& unsat	\\
		\#3	& 5		&  49.279	&  21.836	&  63.565	&  25.299	&  91.997	& 215.889	&  89.636	&   5.140	&  14.849	&  13.539	& unsat	\\
		& 6		& 173.166	&  89.663	& 156.303	&  96.474	& $>$ 600	& $>$ 600	& 150.506	&  16.581	&  31.128	&  28.166	& unsat	\\
		& 7		& 335.292	& 337.237	& $>$ 600	& 188.084	& ---		& ---		& $>$ 600	&  32.838	& 304.836	&  81.453	& unsat	\\
		& 8		& $>$ 600	& $>$ 600	& ---		& $>$ 600	& ---		& ---		& ---		& 139.254	& 451.156	& 404.832	& unsat	\\
		& 9		& ---		& ---		& ---		& ---		& ---		& ---		& ---		& 240.595	& $>$ 600	& $>$ 600	& unsat	\\
		& 10	& ---		& ---		& ---		& ---		& ---		& ---		& ---		& $>$ 600	& ---		& ---		& N/A	\\
		\hline\hline
	\end{tabular}
	
\end{table*}

\section{Summary and Future work}
A symbolic representation for efficient bounded model checking is proposed, and its effectiveness is demonstrated.
An experiment with practical TPNs and a comparison with other tools such as TINA and Romeo are on our future research direction.
We will extend this work to apply interpolation based unbounded model checking (UBMC). \cite{McMillan2003}\cite{Igawa2018}.
Future work is an application the effective UBMC algorithm\cite{Li2006} to our encoding.

\section*{Acknowledgments}
This research was partially supported by JST CREST Grant Number JPMJCR1531, Japan.
A part of the experiments was conducted with the support of the 2017 research grant of the Okawa Foundation for Information and Telecommunications.

\bibliographystyle{ieicetr}
\bibliography{ieice_igawa.bib}

\end{document}